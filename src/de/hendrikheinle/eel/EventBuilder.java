package de.hendrikheinle.eel;

import java.util.ArrayList;
import java.util.List;

public class EventBuilder<T> {
	protected List<Listener<T>> _listener = new ArrayList<>();
	
	public EventBuilder() {
	}
	
	public void addListener(Listener<T> listener) {
		this._listener.add(listener);
	}
	
	public boolean removeListener(Listener<T> listener) {
		return this._listener.remove(listener);
	}
	
	public boolean removeListener(Class<?> cls) {
		int removeCounter = 0;
		for (Listener<T> listener : this._listener) {
			if(listener.getClass().equals(cls)) {
				this._listener.remove(listener);
				removeCounter++;
			}
		}
		return removeCounter > 0;
	}
	
	public void clear() {
		this._listener.clear();
	}
}
