package de.hendrikheinle.eel;

public class EventDispatcher<T> {
	private EventBuilder<T> _builder;

	public EventDispatcher() {
		this._builder = new EventBuilder<T>();
	}

	public EventBuilder<T> getEventBuilder() {
		return this._builder;
	}

	public void dispatch(T args) {

		this._builder._listener.stream().forEach(listener -> listener.dispatch(args));
	}
}
