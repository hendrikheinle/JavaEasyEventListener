package de.hendrikheinle.eel;

public interface Listener<T> {
	public void dispatch(T args);
}
